[![100](https://abs.twimg.com/emoji/v1/72x72/1f4af.png)]()
# [PSYC] Terms Chapter 7

#### Announcement
We wll be trying out an easier way to work with terms, since last time it became very confusing and people did the same term twice. This time you just do the terms listed below for your name.

##### Terms Per Person: `tba`
##### Due Date: `tba`
##### Estimated Completion Time: `tba`

------------------------

#### Last Chapter's Winner

* Mehul - _Finished the fastest_
* Mohamed - _Exception, becuase he did 19 terms_

### Terms To Do

* **[Mehul]()**
* [James]()
* [Alejo]()
* **[Mohamed]()**
* [Win San]()
* [Andrew]()

Those are the symbols, just look for your symbol below:

### List of Terms

#### Mehul

```sh
Term1
```
#### James

```sh
Term1
```
#### Alejo

```sh
Term1
```
#### Mohamed

```sh
Term1
```
#### Win San

```sh
Term1
```
#### Andrew

```sh
Term1
```

